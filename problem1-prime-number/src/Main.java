import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(primeNumber(2));
    }

    public static String primeNumber(int number) {
        if (number < 2) {
            return "Bukan Bilangan Prima";
        }

        int squaredNumber = (int) Math.sqrt((double) number);
        for (int i = 2; i<squaredNumber; i++) {
            if (squaredNumber % i == 0) {
                return "Bukan Bilangan Prima";
            }
        }

        return "Bilangan Prima";
    }


    public static Boolean isPrime(Integer number) {
        Map<Integer, Integer> map = new HashMap<>();

        if (map.containsKey(number)) {
            return false;
        } else {
            return true;
        }
    }
}